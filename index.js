/** BAI TAP 1
 * 
 * Input: Nhập số ngày làm việc của nhân viên
 *      : Nhập mức lượng của một ngày làm việc (100.000 / ngày)
 * 
 * Buoc xu ly: Tao var salaryPerDay = 100000, var dayOfWorks, var totalAmount
 *           : totalAmount = salaryPerDay * dayOfWorks
 *           : print out totalAmount to console log.
 * 
 * Output: Tổng tiền lương nhân viên nhận được tương ứng với số ngày làm việc
 *  
 */

var salaryPerDay = 100000;
var dayOfWorks, totalAmount;

dayOfWorks = 7;

totalAmount = salaryPerDay * dayOfWorks;

console.log("Tiền lương nhận được là", totalAmount);



/**BAI TAP 2
 * 
 * Input: Nhập số thực 1, số thực 2, số thực 3, số thực 4, số thực 5
 *      
 * 
 * Bước xử lý: Tạo biến var numb1, numb2, numb3, numb4, numb5
 *           : Tạo biến var totalNumber và tính tổng của 5 biến trên.
 *           : Tạo biến var result, và lấy biến totalNumber / 5.      
 * 
 * Output: Giá trị trung bình của 5 số thực là biến result.
 *  
 */

var numb1 =1, numb2=2, numb3=3, numb4=4, numb5=5;

var totalNumber = numb1+numb2+numb3+numb4+numb5;
var result = totalNumber/5;

console.log("Gia tri trung binh cua 5 so thuc la: ", result);


/**BAI TAP 3
 * 
 * Input: Nhập số lượng USD cần chuyển đổi
 *      
 * 
 * Bước xử lý: Tạo biến var amount, rate =  23500, result
 *           : Thực hiện thao tác quy đổi USD bằng công thức result = amount * rate      
 * 
 * Output: Giá trị sau khi quy đổi là biến result.
 *  
 */

var result, rate = 23500, amount = 2;

result = amount * rate;

console.log("Ti gia cua ", amount, " USD sau khi quy doi la: ", result);

/**BAI TAP 4
 * 
 * Input: Nhập chiều dài và chiều rộng của hình chữ nhật
 *      
 * 
 * Bước xử lý: Tạo biến var longEdge, shortEdge, perimeterRectangle, areaRectangle
 *           : tính chu vi: perimeterRectangle = (longEdge + shortEdge) * 2;
 *           : tính diện tích: areaRectangle = longEdge * shortEdge;        
 * 
 * Output: Print giá trị chu vi và diện tích lên console.log
 *  
 */

var longEdge = 5, shortEdge = 2, perimeterRectangle, areaRectangle;


perimeterRectangle = (longEdge + shortEdge) * 2;
areaRectangle = longEdge * shortEdge;

console.log("Chu vi hình CN là: ", perimeterRectangle, ". Dien tich hinh CN là: ", areaRectangle);

/**BAI TAP 5
 * 
 * Input: Nhập ký số
 *      
 * 
 * Bước xử lý: Tạo biến var number, sum, unit, ten
 *           : Thực hiện phép tính để lấy số hàng đơn vị: unit = number % 10;
 *           : Thực hiện phép tính để lấy số hàng chục: ten = number / 10;
 *           : Thực hiện phép tổng của result = unit + ten;
 * 
 * Output: Print giá trị result lên console.log
 *  
 */
 var number = 44, sum, unit, ten;

 unit = Math.floor(number%10);
 ten = Math.floor(number/10);

 sum = unit + ten;

 console.log("Tong cac ky so la:", sum);